__author__ = 'Joseph Catrambone'

import sys

import theano
from theano import tensor as T
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
import numpy as np
from theano.tensor.nnet.conv import conv2d
from theano.tensor.signal.downsample import max_pool_2d

from vectorizer import sentence_to_vector, vector_to_sentence

theano.config.floatX = 'float32' # Need to do this for GPU compatibility.

srng = RandomStreams()
MAX_SENTENCE_LENGTH = 140
TRAINING_EXAMPLE_COUNT = 500
TESTING_EXAMPLE_COUNT = 500
BATCH_SIZE = 16

def debug_print(str):
	print(str)

def floatX(X):
	return np.asarray(X, dtype=theano.config.floatX)

def init_weights(shape, weight_scale=0.01):
	return theano.shared(floatX(np.random.randn(*shape) * weight_scale))

def rectify(X):
	return T.maximum(X, 0.)

def softmax(X):
	e_x = T.exp(X - X.max(axis=1).dimshuffle(0, 'x'))
	return e_x / e_x.sum(axis=1).dimshuffle(0, 'x')

def dropout(X, p=0.):
	if p > 0:
		retain_prob = 1 - p
		X *= srng.binomial(X.shape, p=retain_prob, dtype=theano.config.floatX)
		X /= retain_prob
	return X

def RMSprop(cost, params, lr=0.001, rho=0.9, epsilon=1e-6):
	grads = T.grad(cost=cost, wrt=params)
	updates = []
	for p, g in zip(params, grads):
		acc = theano.shared(p.get_value() * 0.)
		acc_new = rho * acc + (1 - rho) * g ** 2
		gradient_scaling = T.sqrt(acc_new + epsilon)
		g = g / gradient_scaling
		updates.append((acc, acc_new))
		updates.append((p, p - lr * g))
	return updates

def build_connections(X, w, w2, w3, w4, p_drop_conv, p_drop_hidden):
	l1a = rectify(conv2d(X, w, border_mode='full'))
	l1 = max_pool_2d(l1a, (1, 4))
	l1 = dropout(l1, p_drop_conv)

	l2a = rectify(conv2d(l1, w2))
	l2 = max_pool_2d(l2a, (1, 4))
	l2 = dropout(l2, p_drop_conv)

	l3a = rectify(conv2d(l2, w3))
	l3b = max_pool_2d(l3a, (1, 2))
	l3 = T.flatten(l3b, outdim=2)
	l3 = dropout(l3, p_drop_conv)

	l4 = rectify(T.dot(l3, w4))
	l4 = dropout(l4, p_drop_hidden)

	pyx = softmax(T.dot(l4, w_o))
	return l1, l2, l3, l4, pyx

if __name__ == "__main__":
	debug_print("Loading data.")
	fin = open(sys.argv[1], 'r')
	lines = fin.readlines()
	fin.close()

	# Create the training data
	debug_print("Vectorizing dataset.")
	training_data = [sentence_to_vector(line, min_length=MAX_SENTENCE_LENGTH, max_length=MAX_SENTENCE_LENGTH) for line in lines[:TRAINING_EXAMPLE_COUNT+TESTING_EXAMPLE_COUNT]]
	example = training_data[0]
	trX = floatX(training_data[0:TRAINING_EXAMPLE_COUNT])
	teX = trX # We're making an autoencoder, so the output should be the input
	trY = floatX(training_data[TRAINING_EXAMPLE_COUNT:TRAINING_EXAMPLE_COUNT+TESTING_EXAMPLE_COUNT])
	teY = trY

	trX = trX.reshape(-1, 1, 1, example.shape[0])
	teX = teX.reshape(-1, 1, 1, example.shape[0])

	# Build network
	debug_print("Building network.")
	X = T.ftensor4()
	Y = T.fmatrix()

	w = init_weights((1024, 1, 1, example.shape[0])) # 32, 1, 3, 3 # 140*96/16
	w2 = init_weights((64, 1, 1, 1024)) # 64, 32, 3, 3
	w3 = init_weights((128, 1, 1, 64))
	w4 = init_weights((128 * 1 * 1, 128))
	w_o = init_weights((128, example.shape[0]))

	noise_l1, noise_l2, noise_l3, noise_l4, noise_py_x = build_connections(X, w, w2, w3, w4, 0.2, 0.5)
	l1, l2, l3, l4, py_x = build_connections(X, w, w2, w3, w4, 0., 0.)
	y_x = T.argmax(py_x, axis=1)

	cost = T.mean(T.nnet.categorical_crossentropy(noise_py_x, Y))
	params = [w, w2, w3, w4, w_o]
	updates = RMSprop(cost, params, lr=0.001)

	train = theano.function(inputs=[X, Y], outputs=cost, updates=updates, allow_input_downcast=True)
	predict = theano.function(inputs=[X], outputs=y_x, allow_input_downcast=True)

	# Train
	debug_print("Training network")
	for i in range(10):
		for start, end in zip(range(0, len(trX), BATCH_SIZE), range(BATCH_SIZE, len(trX), BATCH_SIZE)):
			debug_print("Training examples '{}'".format(vector_to_sentence(trX[start][0][0])))
			cost = train(trX[start:end], trY[start:end])
		print(vector_to_sentence(predict(teX[start][0][0])))
