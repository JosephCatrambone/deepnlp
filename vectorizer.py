__author__ = 'Joseph Catrambone'

import numpy
from random import random, choice

DEFAULT_CHUNK_SIZE = 4
LAST_CHARACTER = 129 # 126 = ~, 127 = skip, 128 = end of sentence, 129 = OOB
FIRST_CHARACTER = 32 # <space>
INVALID_CHARACTER = 127
TERMINATOR_CHARACTER = 128
CHARACTER_VECTOR_SIZE = LAST_CHARACTER-FIRST_CHARACTER
DATATYPE = numpy.float

def character_to_vector(c):
	vector = numpy.zeros(CHARACTER_VECTOR_SIZE, dtype=DATATYPE)
	val = ord(c)
	if val < FIRST_CHARACTER:
		val = FIRST_CHARACTER
	elif val >= LAST_CHARACTER:
		val = FIRST_CHARACTER
	vector[val - FIRST_CHARACTER] = 1.0
	return vector

def vector_to_character(v):
	"""Given a vector of probabilities, select a character with the same distribution as the input vector."""
	choice = random()
	for i in range(0, v.shape[0]):
		if choice < v[i]:
			return chr(FIRST_CHARACTER+i)
		else:
			choice -= v[i]
	# If we are here, choice was greater than all probabilities, so likely all of v[i] was 0.
	# So return an empty character.
	return '' # Entirely empty vector

def sentence_to_vector(sentence, min_length=0, max_length=160):
	"""Take an entire sentence and build a vector from it.  No overlap by default."""
	vector = list()
	# To make sure our vectors are the same size, blank fill them if they're less than minimum
	sentence_padded = sentence
	if min_length > 0 and len(sentence) < min_length:
		sentence_padded = sentence + (chr(0)*(min_length-len(sentence)))
	sentence_padded = sentence_padded[:max_length]
	# Convert each character to a vector
	for c in sentence_padded:
		subvector = numpy.zeros(CHARACTER_VECTOR_SIZE, dtype=DATATYPE)
		val = ord(c)
		if val < FIRST_CHARACTER:
			val = INVALID_CHARACTER
		elif val >= LAST_CHARACTER:
			val = INVALID_CHARACTER
		subvector[val - FIRST_CHARACTER] = 1.0
		
		vector.append(subvector)
	# Add a terminating character to the end.
	endcap = numpy.zeros(CHARACTER_VECTOR_SIZE, dtype=DATATYPE)
	endcap[TERMINATOR_CHARACTER-FIRST_CHARACTER] = 1.0
	vector.append(endcap)
	# And concatenate the vectors
	vector = numpy.asarray(vector, dtype=DATATYPE)
	return numpy.reshape(vector, (1, -1))[0] # Convert from a 2d matrix to a 1D vector

def vector_to_sentence(vector):
	string = ""
	for i in range(0, vector.shape[0], CHARACTER_VECTOR_SIZE):
		#string += vector_to_character(vector[i:i+CHARACTER_VECTOR_SIZE])
		choice = random()
		choice_made = False
		j = 0
		while not choice_made and j < CHARACTER_VECTOR_SIZE:
			if choice < vector[j+i]:
				if j+FIRST_CHARACTER == TERMINATOR_CHARACTER:
					return string # We found the kill character
				elif j+FIRST_CHARACTER == INVALID_CHARACTER or j+FIRST_CHARACTER == LAST_CHARACTER:
					pass
				else:
					string += chr(FIRST_CHARACTER+j)
				choice_made = True
			else:
				choice -= vector[j+i]
			j += 1
	return string

def test_character():
	for _ in range(50):
		c = choice(" ~abcdefgASDBHUAUILHWA:\"#*(@P#H*(")
		c_prime = vector_to_character(character_to_vector(c))
		if c != c_prime:
			print("Test fail: {} != {}".format(c, c_prime))
			return False
	print("Test pass")
	return True

def test_sentence():
	def make_string():
		return "".join([choice(" ~abcdefg") for _ in range(40)])

	sent = make_string()
	sent2 = vector_to_sentence(sentence_to_vector(sent))
	if sent == sent2:
		print("Test pass")
		return True
	else:
		print("Test fail:\n{}\n{}".format(sent, sent2))
		return False

if __name__=="__main__":
	# If run as a script, do tests.
	test_character()
	test_sentence()